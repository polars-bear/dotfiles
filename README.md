# dotfiles



## Getting started

The dotfiles are structured for GNU stow. Use `make` to stow everything and `make delete` to delete all symlinks.

Or manually stow each package:
```
stow -v --target=$HOME */
```
