#!/bin/zsh
# ================================================= #
# d8888b.  .d88b.  db       .d8b.  d8888b. .d8888.  #
# 88  `8D .8P  Y8. 88      d8' `8b 88  `8D 88'  YP  #
# 88oodD' 88    88 88      88ooo88 88oobY' `8bo.    #
# 8888P   88    88 88      8888888 88`8b     `Y8b.  #
# 88      `8b  d8' 88booo. 88   88 88 `88. db   8D  #
# 88       `Y88P'  Y88888P YP   YP 88   YD `8888Y'  #
# ================================================= #

# ================================================= #
# PROGRAMS & PATH
# ================================================= #

# set default programs
export EDITOR="hx"
export BROWSER="firefox"
export TERMINAL="wezterm"
export SHELL="/usr/bin/zsh"
export TERM="screen-256color"

# extend PATH
export PATH="$PATH:$HOME/bin"
export PATH="$PATH:$HOME/.local/bin"
export PATH="$PATH:$HOME/.local/bin/jdt-ls-latest/bin"

export HOMEBREW_PREFIX="/home/linuxbrew/.linuxbrew";
export HOMEBREW_CELLAR="/home/linuxbrew/.linuxbrew/Cellar";
export HOMEBREW_REPOSITORY="/home/linuxbrew/.linuxbrew/Homebrew";
export PATH="/home/linuxbrew/.linuxbrew/bin:/home/linuxbrew/.linuxbrew/sbin${PATH+:$PATH}";
[ -z "${MANPATH-}" ] || export MANPATH=":${MANPATH#:}";
export INFOPATH="/home/linuxbrew/.linuxbrew/share/info:${INFOPATH:-}";
[ -z "${XDG_DATA_DIRS-}" ] || export XDG_DATA_DIRS="/home/linuxbrew/.linuxbrew/share:${XDG_DATA_DIRS-}";

# ================================================= #
# PLUGINS & OPTIONS 
# ================================================= #

# source custom functions
source "$ZDOTDIR/zsh-functions"

zsh_add_file "zsh-aliases"
zsh_add_file "zsh-vim-mode"

# plugins
zsh_add_plugin "zsh-users/zsh-autosuggestions"
zsh_add_plugin "zsh-users/zsh-syntax-highlighting"

# set options
setopt menucomplete
setopt extendedglob
setopt interactive_comments

# use colors
autoload -U colors && colors

# configure history
HISTSIZE=10000
SAVEHIST=$HISTSIZE
HISTDUP=erase
HISTFILE="$ZDOTDIR/.zsh_history"
setopt appendhistory
setopt sharehistory
setopt hist_expire_dups_first
setopt hist_ignore_dups
setopt hist_ignore_all_dups
setopt hist_ignore_space
setopt hist_find_no_dups
setopt hist_save_no_dups


# ================================================= #
# COMPLETIONS & KEYBINDS
# ================================================= #

# completions
autoload -Uz compinit
zstyle ':completion:*' menu select
zstyle ':completion::complete:lsof:*' menu yes select

zmodload zsh/complist
_comp_options+=(globdots)		# Include hidden files.
compinit

# use Ctrl+k and Ctrl+j to scroll history
autoload -U up-line-or-beginning-search
autoload -U down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search

bindkey "^k" up-line-or-beginning-search # Up in history
bindkey "^j" down-line-or-beginning-search # Down in history

# edit line in editor with Ctrl+e
autoload edit-command-line
zle -N edit-command-line
bindkey '^e' edit-command-line

# search history
bindkey '^R' history-incremental-search-backward

# go to next command with same name
bindkey '^p' history-search-backward
bindkey '^n' history-search-forward

# ================================================= #
# GIT INFO
# ================================================= #

# show git repo and changes (zsh can be slow in repos)
# from https://github.com/zsh-users/zsh/blob/master/Misc/vcs_info-examples
autoload -Uz vcs_info
zstyle ':vcs_info:*' enable git svn
precmd_vcs_info() { vcs_info }
precmd_functions+=( precmd_vcs_info )

# add a function to check for untracked files in the directory.
zstyle ':vcs_info:git*+set-message:*' hooks git-untracked

+vi-git-untracked(){
    if [[ $(git rev-parse --is-inside-work-tree 2> /dev/null) == 'true' ]] && \
        git status --porcelain | grep '??' &> /dev/null ; then
        # This will show the marker if there are any untracked files in repo.
        hook_com[staged]+='*' # signify new files with a bang
    fi
}

zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:git*' formats "%F{11}[%b]%F{9}%m%u%c%{$reset_color%}"


# ================================================= #
# PROMPT
# ================================================= #

# set PROMPT
setopt prompt_subst

# Prompt: user@pwd[git]*
# PROMPT='%B%F{10}%n %F{5}@ %F{4}%~ %{$reset_color%}${vcs_info_msg_0_} %b'
PROMPT='%B%F{2}%n %F{5}@ %F{4}%~ %{$reset_color%}${vcs_info_msg_0_} %b'

# prompt: user@pwd
# PROMPT='%B%F{10}%n%F{5}@%F{4}%~%{$reset_color%}%b '

# show execution time of last command
threshold=2
function preexec() {
  secs=$SECONDS
}

function precmd() {
  if [ $secs ]; then
    secs_diff=$(($SECONDS - $secs))

    if [ $secs_diff -gt 60 ]; then
        mins=$(($secs_diff/60))
        secs_rem=$(($secs_diff%60))
        export RPROMPT="%B%F{yellow}${mins}m ${secs_rem}s%{$reset_color%}%b"
    elif [ $secs_diff -gt $threshold ]; then
        export RPROMPT="%B%F{yellow}${secs_diff}s%{$reset_color%}%b"
    else
      export RPROMPT=""
    fi

    unset secs
  fi
}

# add newline to prompt for command input
PROMPT+=$'\n%B%F{yellow}> %{$reset_color%}%b'
