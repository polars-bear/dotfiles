-- ================================================= --
-- d8888b.  .d88b.  db       .d8b.  d8888b. .d8888.  --
-- 88  `8D .8P  Y8. 88      d8' `8b 88  `8D 88'  YP  --
-- 88oodD' 88    88 88      88ooo88 88oobY' `8bo.    --
-- 8888P   88    88 88      8888888 88`8b     `Y8b.  --
-- 88      `8b  d8' 88booo. 88   88 88 `88. db   8D  --
-- 88       `Y88P'  Y88888P YP   YP 88   YD `8888Y'  --
-- ================================================= --

-- Pull in the wezterm API
local wezterm = require 'wezterm'

-- This will hold the configuration.
local config = wezterm.config_builder()
local act = wezterm.action

config.color_scheme = 'Monokai (terminal.sexy)'
-- config.font = wezterm.font 'Fira Mono Nerdfont'
config.font_size = 14

config.use_fancy_tab_bar = false
config.tab_max_width = 48

config.colors = {
  tab_bar = {
    active_tab = {
          -- The color of the background area for the tab
          bg_color = '#2c1775',
          -- The color of the text for the tab
          fg_color = '#c0c0c0',

          -- "Half", "Normal" or "Bold" intensity
          intensity = 'Bold',
    },
  }
}

-- define leader key combo
-- timeout_milliseconds defaults to 1000 and can be omitted
config.leader = { key = 'a', mods = 'CTRL', timeout_milliseconds = 1000 }

config.keys = {
  -- Send "CTRL-A" to the terminal when pressing CTRL-A, CTRL-A
  {
    key = 'a',
    mods = 'LEADER|CTRL',
    action = act.SendKey { key = 'a', mods = 'CTRL' },
  },
  {
    key = 's',
    mods = 'LEADER|CTRL',
    action = act.SplitHorizontal { domain = 'CurrentPaneDomain' },
  },
  {
    key = 'v',
    mods = 'LEADER|CTRL',
    action = act.SplitVertical { domain = 'CurrentPaneDomain' },
  },
  {
    key = 't',
    mods = 'LEADER|CTRL',
    action = act.ActivateLastTab,
  },
  {
    key = 'n',
    mods = 'CTRL',
    action = act.CopyMode { MoveForwardZoneOfType = 'Output' },
  },
  {
    key = 'p',
    mods = 'CTRL',
    action = act.CopyMode { MoveBackwardZoneOfType = 'Output' },
  },
  {
    key = 'z',
    mods = 'CTRL',
    action = act.CopyMode { SetSelectionMode = 'SemanticZone' },
  },
}

for i = 1, 9 do
  -- LEADER + number to activate that tab
  table.insert(config.keys, {
    key = tostring(i),
    mods = 'LEADER',
    action = act.ActivateTab(i - 1),
  })
end

-- and finally, return the configuration to wezterm
return config
